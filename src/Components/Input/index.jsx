import { useState, useEffect } from "react";

const Input = ({ pegarInformacoes, validacao }) => {
  const [name, setName] = useState("");
  const [nameSearch, setNameSearch] = useState("");
  const [validateRepository, setValidateRepository] = useState("");
  //const [validateEmpty, setValidateEmpty] = useState("vazio");

  useEffect(() => {
    //busca API inicial evitando erro
    if (nameSearch === "") {
      pegarInformacoes(`https://api.github.com/`);
    } else if (nameSearch.search("/") !== -1) {
      //Pega a organização antesDaBarra/
      const indiceBarra = nameSearch.search("/");
      const organizacao = nameSearch.substr(0, indiceBarra);

      //Pega o repositório /apósABarra
      const repositorio = nameSearch.substring(
        indiceBarra + 1,
        nameSearch.length
      );

      //valida se foi digitado o repositorio
      if (nameSearch.substring(indiceBarra + 1) !== "") {
        setValidateRepository("");
        pegarInformacoes(
          `https://api.github.com/repos/${organizacao}/${repositorio}`
        );
      }
    } else {
      const organizacao = nameSearch;
      pegarInformacoes(`https://api.github.com/repos/${organizacao}`);
      setValidateRepository("vazio");
    }
  }, [nameSearch]);

  return (
    <div>
      <input
        placeholder="Digite: nome-do-usuario/repositório"
        onChange={(e) => {
          setName(e.target.value);
        }}
      ></input>

      <button onClick={() => setNameSearch(name)}>Pesquisar</button>

      {validateRepository === "vazio" ? (
        <div> Por favor, digite nome-do-usuario/repositório </div>
      ) : validacao.message === "Not Found" ? (
        <div>{validacao.message}</div>
      ) : (
        ""
      )}
    </div>
  );
};

export default Input;
