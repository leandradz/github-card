import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import useStyles from "./styles";

export default function MediaCard({ informacoes }) {
  const classes = useStyles();

  return informacoes.owner !== undefined ? (
    <Card className={classes.root}>
      <CardActionArea>
        <a
          className={classes.links}
          href={informacoes.html_url}
          target="_blank"
          rel="noreferrer"
        >
          <CardMedia
            className={classes.media}
            image={informacoes.owner.avatar_url}
            title="avatar da aplicação atual"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" color="black" component="h2">
              {informacoes.name}
            </Typography>
            <Typography variant="body2" color="black" component="p">
              {informacoes.description}
            </Typography>
          </CardContent>
        </a>
      </CardActionArea>
    </Card>
  ) : (
    ""
  );
}
