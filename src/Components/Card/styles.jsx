import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    marginTop: 10,
  },
  media: {
    backgroundSize: "contain",
    height: 140,
    padding: 10,
  },
  links: {
    textDecoration: "none",
    color: "black",
  },
});

export default useStyles;
