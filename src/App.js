import "./App.css";
import Input from "./Components/Input";
import Card from "./Components/Card";
import { useState } from "react";

const GitHubCard = () => {
  const [informacoes, setInformacoes] = useState("");

  const pegarInformacoes = (url) => {
    fetch(url)
      .then((response) => response.json())
      .then((response) => setInformacoes(response));
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Pesquise seu repositório favorito do github</h1>

        <Input pegarInformacoes={pegarInformacoes} validacao={informacoes} />

        <Card informacoes={informacoes} />
      </header>
    </div>
  );
};

export default GitHubCard;
